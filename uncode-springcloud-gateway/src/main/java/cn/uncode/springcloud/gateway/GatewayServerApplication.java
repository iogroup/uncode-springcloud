package cn.uncode.springcloud.gateway;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import cn.uncode.springcloud.starter.boot.app.AppInfo;
import cn.uncode.springcloud.starter.boot.app.UncodeApplication;

/**
 * @author juny
 */
@SpringBootApplication
public class GatewayServerApplication {

    public static void main(String[] args) {
    	UncodeApplication.run(AppInfo.APPLICATION_GATEWAY, GatewayServerApplication.class, args);
    }

}
