package cn.uncode.springcloud.starter.boot.app;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;

public final class AppInfo {
	
	private static final AppInfo ME = new AppInfo();
	
	private AppInfo() {}
	
	private Environment env;
	
	public static AppInfo me() {
		return ME;
	}
	
	public static Environment getEnv() {
		return ME.env;
	}
	
	public static void setEnv(Environment env) {
		ME.env = env;
	}
	
	public static String getAppName() {
		String appName = ME.env.getProperty("spring.application.name");
		if(StringUtils.isBlank(appName)) {
			appName = ME.env.getProperty("app.name");
		}
		if(StringUtils.isBlank(appName)) {
			appName = ME.env.getProperty("app.id");
		}
		return appName;
	}
	
	//-----------------------------------------------------------
	// 框架常量
	//-----------------------------------------------------------
	/**
	 * 框架版本
	 */
	public static final String FRAMEWORK_VERSION = "1.0.0";
	/**
	 * 代码部署于 linux 上，工作默认为 mac 和 Windows
	 */
	public static final String FRAMEWORK_OS_NAME_LINUX = "LINUX";
	/**
	 * 灰度标识
	 */
	public static final String APPLICATION_VERSION_METADATA_KEY = "canaryFlag";
	
	
	//-----------------------------------------------------------
	// 基础应用名称
	//-----------------------------------------------------------
	public static final String APPLICATION_EUREKA = "uncode-eureka";
	public static final String APPLICATION_GATEWAY = "uncode-gateway";
	public static final String APPLICATION_ADMIN = "uncode-admin";



	
	private Map<String, String> applications = new HashMap<>();
	
	static {
		ME.applications.put(APPLICATION_GATEWAY, "网关");
		ME.applications.put(APPLICATION_ADMIN, "监控后台");
		ME.applications.put(APPLICATION_EUREKA, "注册中心");
	}
	
	public static String getAppDescription() {
		return ME.applications.get(getAppName());
	}
	
	//-----------------------------------------------------------
	// 开发环境
	//-----------------------------------------------------------
	/**
	 * 环境
	 * @author juny
	 * @date 2019年2月1日
	 *
	 */
	public enum Profile {
        DEV("dev", "开发"),
        FAT("fat", "测试"),
        PRE("pre", "预生产"),
        PRD("prd", "生产");

        Profile(String name, String description){
			this.name = name;
			this.description = description;
		}
		String name;
		String description;

	}
	

	//-----------------------------------------------------------
	// 三方组件常量
	//-----------------------------------------------------------
	/**
	 * sentinel的apollo命名空间
	 */
	public static final String SENTINEL_APOLLO_NAMESPACE = "application";
	/**
	 * sentinel的默认规则
	 */
	public static final String SENTINEL_DEFAULT_FLOW_RULES = "[]";
	/**
	 * apollo的app.id
	 */
	public static final String APOLLO_APP_ID = "app.id";


	//---------------------------------------------------
    //sentinel资源定义
	//---------------------------------------------------
    public static final String RESOURCE_COMMON="RESOURCE_COMMON";


	
	

}
