package cn.uncode.springcloud.admin.dal.system;

import org.springframework.stereotype.Service;

import cn.uncode.springcloud.admin.model.system.dto.DeskIconDTO;

import cn.uncode.dal.external.AbstractCommonDAL;

 /**
 * service类,此类由Uncode自动生成
 * @author uncode
 * @date 2019-05-14
 */
@Service
public class DeskIconDALImpl extends AbstractCommonDAL<DeskIconDTO> implements DeskIconDAL {

}