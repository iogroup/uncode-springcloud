package cn.uncode.springcloud.admin.model;

import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @auth:qiss
 * @see: [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@Data
public class ValidateBean {
    private String name;
    @NotNull
    private Long age;
    private String email;
    private String mobile;

    private String realName;

    private String cardId;

}
