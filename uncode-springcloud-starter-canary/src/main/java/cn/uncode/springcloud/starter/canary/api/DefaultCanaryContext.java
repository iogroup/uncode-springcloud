package cn.uncode.springcloud.starter.canary.api;

import cn.uncode.springcloud.starter.canary.api.CanaryContext;
import cn.uncode.springcloud.starter.canary.api.CanaryStrategy;

/**
 * 
 * @author juny
 * @date 2019年1月21日
 *
 */
public class DefaultCanaryContext implements CanaryContext {
	
	private CanaryStrategy canaryStrategy;
	
	public DefaultCanaryContext(CanaryStrategy canaryStrategy) {
		this.canaryStrategy = canaryStrategy;
	}

	@Override
	public CanaryStrategy getCanaryStrategy() {
		return canaryStrategy;
	}

}
